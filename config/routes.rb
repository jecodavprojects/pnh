Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self) rescue ActiveAdmin::DatabaseHitDuringLoad
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'statics#index'
  resources :statics  do
    collection do
      get   'about_us'
      get   'admissions'
      get   'announcements'
      get   'facilities'
      get   'photo_galleries'
    end
  end
  match '/home',   to: 'statics#index',   via: 'get'
  match '/about_us',   to: 'statics#about_us',   via: 'get'
  match '/mission_vision',   to: 'statics#mission_vision',   via: 'get'
  match '/admissions',   to: 'statics#admissions',   via: 'get'
  match '/announcements',   to: 'statics#announcements',   via: 'get'
  match '/facilities',   to: 'statics#facilities',   via: 'get'
  match '/photo_galleries',   to: 'statics#photo_galleries',   via: 'get'
  match '/admission/:id',   to: 'statics#show',   via: 'get',   as: 'admission'
  match '/announcement/:id',   to: 'statics#show',   via: 'get',   as: 'announcement'
  match '/post/:id',   to: 'statics#show',   via: 'get',   as: 'post'
end
