# Change these
# server 'ubuntu@scoutlook.opinionated.software', roles: [:web, :app, :db, :worker, :prod], primary: true
# server 'ubuntu@99.79.186.43', roles: [:web, :app, :db, :non_worker, :prod]
server 'root@156.67.214.10', roles: [:web, :app, :db], primary: true

set :repo_url,        'https://jericolopez1993@bitbucket.org/jecodavprojects/pnh.git'
set :git_http_username, 'jericolopez1993'
set :git_http_password, 'rambo0702'
set :application,     'pnh'
set :user,            'root'
set :password,        'FG9RGkR6R6Wm@'
set :puma_threads,    [4, 16]
set :puma_workers,    0
set :branch, ENV.fetch('REVISION', 'master')
set :rvm_ruby_version, '2.6.0'
set :ssh_options, paranoid: false


# Don't change these unless you know what you're doing
set :pty,             true
set :use_sudo,        false
set :stage,           :production
set :deploy_via,      :remote_cache
set :deploy_to,       "/home/#{fetch(:user)}/apps/#{fetch(:application)}"
set :puma_bind,       "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"
set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"
set :ssh_options,     { forward_agent: true, user: fetch(:user), password: fetch(:password) }
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true  # Change to false when not using ActiveRecord
set :keep_releases, 1
set :linked_dirs, %w{public/system storage}
