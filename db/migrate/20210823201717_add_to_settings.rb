class AddToSettings < ActiveRecord::Migration[5.2]
  def up
    add_column :settings, :menu_7, :string
    add_column :settings, :menu_8, :string
  end

  def down
    remove_column :settings, :menu_7
    remove_column :settings, :menu_8
  end
end
