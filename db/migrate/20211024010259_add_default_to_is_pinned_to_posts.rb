class AddDefaultToIsPinnedToPosts < ActiveRecord::Migration[5.2]
  def self.up
    change_column :posts, :is_pinned, :boolean, :default => false
  end

  def self.down
    # You can't currently remove default values in Rails
    raise ActiveRecord::IrreversibleMigration, "Can't remove the default"
  end
end
