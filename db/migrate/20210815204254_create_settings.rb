class CreateSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :settings do |t|
      t.string :title
      t.string :short_title
      t.text :description
      t.string :menu_1
      t.string :menu_2
      t.string :menu_3
      t.string :menu_4
      t.string :menu_5
      t.string :menu_6
      t.string :address
      t.string :email_address
      t.string :contact_number_1
      t.string :contact_number_2

      t.timestamps
    end
  end
end
