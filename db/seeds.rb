# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

AdminUser.delete_all
user = AdminUser.create!([
                           {email: 'admin@nsoepassam.ac.pg', password: '@DminPassam', password_confirmation: '@DminPassam'},
                           {email: 'davy@nsoepassam.ac.pg', password: 'D@vyPassam', password_confirmation: 'D@vyPassam'}
                         ])

user = AdminUser.first

Setting.delete_all
setting = Setting.create!(
  title: Setting::DEFAULTS['title'],
  short_title: Setting::DEFAULTS['short_title'],
  description: Setting::DEFAULTS['description'],
  menu_1: Setting::DEFAULTS['menu_1'],
  menu_2: Setting::DEFAULTS['menu_2'],
  menu_3: Setting::DEFAULTS['menu_3'],
  menu_4: Setting::DEFAULTS['menu_4'],
  menu_5: Setting::DEFAULTS['menu_5'],
  menu_6: Setting::DEFAULTS['menu_6'],
  menu_7: Setting::DEFAULTS['menu_7'],
  menu_8: Setting::DEFAULTS['menu_8'],
  address: Setting::DEFAULTS['address'],
  email_address: Setting::DEFAULTS['email_address'],
  contact_number_1: Setting::DEFAULTS['contact_number_1'],
  contact_number_2: Setting::DEFAULTS['contact_number_2'])

Post.delete_all
Post.create([
              {title: 'Sup Island Primary School', description: 'Students Reading lesson', user_id: user.id, section_id: 1},
              {title: 'Kaindi Demonstration School', description: 'Students Reading lesson', user_id: user.id, section_id: 1},
              {title: 'Kaindi Demonstration School', description: 'Students pose for a class photo', user_id: user.id, section_id: 1},
              {title: 'About East Sepik', description: 'East Sepik Province varies in environment, people and culture.', user_id: user.id, section_id: 2},
              {
                title: 'About Us',
                description: "<p><b>The people and the land</b><br/> East Sepik Province occupies 43,700 km2. It occupies fewer then 10percent of PNG’s total land area. The Province extends for some 190 kilometers along the northern coastline, westward from the boundary with Madang Province to the Sandaun Province and from the inland to the boundaries with Western Highlands and Enga Provinces in the central ranges. The Province includes the Sepik River basin and the off-lying islands of the Wewak and the Schouten Islands groups.</p><p><b>Economic</b><br/> Excess to service centers often requires 4 to 8 hours travelling. Law income scenarios are changing with favorable impact on the provincial economy being experienced due to cocoa and vanilla farming and harvesting. Rural economy is set to improve further with the national highway re-gravelling and sealing programme and other impact projects planned and driven by government such as tuna factory and cocoa rejuvenation program.</p><p><b>Population</b><br/> The province has a population of over 389,000 with a population growth rate of 2.2percent per year, (2000 census). The coasts, the ranges and the foothill areas have higher population densities whilst most other areas, especially the Sepik plain, are sparsely populated. (East Sepik Cooperate Plan - May 2004) <br/><br/> East Sepik is relatively a high fertility province and will continue to grow unless there is a decline in the dependent population as indicated by the results of the 2000 census. The Province has a young population of 44percent under the age of 15 and 3percent aged 65 years and over. This indicates a high dependency ratio of 87percent. The young population in the province already shows potential population growth, which in turn will demand expansion and improvement in education services.</p>",
                user_id: user.id,
                section_id: 5
              },
              {
                title: 'Mission & Vision',
                description: "<p><b>Mission</b><br/> PASSAM NATIONAL HIGH SCHOOL STRIVES TO EDUCATE BETTER CITIZENS BY PROMOTING INTERGRAL HUMAN DEVELOPMENT IN PARTNERSHIP WITH ALL STAKE HOLDERS THROUGH THE PROVISION OF MORAL, RELEVANT AND QUALITY EDUCATION.</p> <p><b>Vision</b><br/> EXCELLENCE THROUGH SELF DISCIPLINE FOR A BETTER TOMORROW</p>",
                user_id: user.id,
                section_id: 6
              }
            ])