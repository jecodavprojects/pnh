class StaticsController < ApplicationController
  def index

  end

  def show
    @post = Post.find(params[:id])
    render 'show_post'
  end

  def about_us
    @post = Post.about_us
  end

  def mission_vision
    @post = Post.mission_vision
  end

  def admissions
    @posts = Post.admissions
  end

  def announcements
    @posts = Post.announcements
  end

  def facilities
    @posts = Post.facilities
  end

  def photo_galleries
    @posts = Post.photo_galleries
  end

  def create
    UserMailer.with(name: params[:name], email: params[:email], phone: params[:phone], message: params[:message]).send_contact.deliver_now
    head :no_content
  end
end
