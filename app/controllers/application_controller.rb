class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session
  before_action :get_settings


  def get_settings
    @setting = Setting.first
  end
end
