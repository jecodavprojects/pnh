ActiveAdmin.register Post, as: 'Facilities' do
  menu false
  # menu priority: 4, label: proc{ Setting.get_settings_value('menu_4') }

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :title, :description, :user_id, image_attachment: []
  #
  # or
  #
  # permit_params do
  #   permitted = [:title, :description, :user_id]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  controller do
    def scoped_collection
      end_of_association_chain.where(:section_id => 3)
    end
  end

  member_action :delete_image_attachment, method: :delete do
    ActiveStorage::Attachment.find(params[:id]).purge

    flash[:notice] = 'Successfully removed image attachment'
    redirect_back(fallback_location: params[:fallback_location])
  end

  index :title => "#{Setting.get_settings_value('menu_4')}" do
    column :title
    column "Description" do |p|
      truncate_html_length_to_sentence(raw(p.description), 500)
    end
    column "Created By" do |p|
      usr = AdminUser.find(p.user_id)
      link_to usr.email, admin_admin_user_path(p.user_id)
    end
    column :created_at
    actions
  end

  show :title => proc{ Setting.get_settings_value('menu_4') } do |post|
    attributes_table do
      row :title
      row "Created By" do |p|
        usr = AdminUser.find(p.user_id)
        link_to usr.email, admin_admin_user_path(p.user_id)
      end
      row :created_at
      row :updated_at
      row "Description" do |p|
        raw(p.description)
      end
    end
    panel "Image Attachment" do
      table_for post.image_attachment do
        column "Actions" do |p|
          span link_to 'Remove', delete_image_attachment_admin_facility_url(:id => p.id, :fallback_location => admin_facility_path(post.id)),
                       method: :delete,
                       data: { confirm: 'Are you sure?' }
        end
        column "Preview" do |p|
          span image_tag p
        end
        column :filename
        column :created_at
      end
    end
  end

  filter :title
  filter :description
  filter :user_id, :as => :select, :collection => AdminUser.all.map{|u| [u.email, u.id]}
  filter :created_at

  form :title => proc{ "Edit #{Setting.get_settings_value('menu_4')}" } do |f|
    f.inputs do
      f.input :image_attachment, label: "Attachment", as: :file, input_html: { multiple: true }
      f.input :title
      f.input :description, as: :html_editor
      f.input :section_id, :input_html => { :value => 3 }, as: :hidden
      f.input :user_id, :input_html => { :value => current_admin_user.id }, as: :hidden
    end
    f.actions

    panel "Image Attachment" do
      table_for f.object.image_attachment do
        column "Actions" do |p|
          span link_to 'Remove', delete_image_attachment_admin_facility_url(:id => p.id, :fallback_location => admin_facility_path(f.object.id)),
                       method: :delete,
                       data: { confirm: 'Are you sure?' }
        end
        column "Preview" do |p|
          span image_tag p
        end
        column :filename
        column :created_at
      end
    end
  end

end
