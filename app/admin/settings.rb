ActiveAdmin.register Setting do
  menu priority: 1

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :title, :short_title, :description, :menu_1, :menu_2, :menu_3, :menu_4, :menu_5, :menu_6, :menu_7, :menu_8, :advisor_message, :image_attachment, :logo_attachment, :background_attachment, :address, :email_address, :contact_number_1, :contact_number_2
  actions :show, :edit, :update

  controller do
    def index
      setting = Setting.first
      redirect_to(admin_setting_url(setting))
    end
  end

  member_action :delete_logo_attachment, method: :delete do
    setting = Setting.first
    if !setting.logo_attachment.nil?
      if setting.logo_attachment.attached?
        ActiveStorage::Attachment.find(setting.logo_attachment.id).purge
      end
    end
    flash[:notice] = 'Successfully removed image attachment'
    redirect_back(fallback_location: params[:fallback_location])
  end

  member_action :delete_background_attachment, method: :delete do
    setting = Setting.first
    if !setting.background_attachment.nil?
      if setting.background_attachment.attached?
        ActiveStorage::Attachment.find(setting.background_attachment.id).purge
      end
    end
    flash[:notice] = 'Successfully removed image attachment'
    redirect_back(fallback_location: params[:fallback_location])
  end

  show do |setting|
    attributes_table do
      row "Logo" do
        if !setting.logo_attachment.nil?
          if setting.logo_attachment.attached?
            span image_tag setting.logo_attachment
            span link_to 'Remove', delete_logo_attachment_admin_setting_url(:id => setting.id, :fallback_location => admin_setting_path(setting.id)),
                         method: :delete,
                         data: { confirm: 'Are you sure?' }
          end
        end
      end
      row "Header Background" do
        if !setting.background_attachment.nil?
          if setting.background_attachment.attached?
            span image_tag setting.background_attachment
            span link_to 'Remove', delete_background_attachment_admin_setting_url(:id => setting.id, :fallback_location => admin_setting_path(setting.id)),
                         method: :delete,
                         data: { confirm: 'Are you sure?' }
          end
        end
      end
      row :title
      row :short_title
      row "Description" do |p|
        raw(p.description)
      end
      row :menu_1
      row :menu_2
      row :menu_3
      row :menu_5
      row :menu_6
      row :menu_7
      row :menu_8
      row :address
      row :email_address
      row :contact_number_1
      row :contact_number_2
    end
  end

  filter :title
  filter :description

  form do |f|
    f.inputs do
      f.input :logo_attachment, label: "Logo", as: :file, :hint => (
        (
          li class: 'file input optional' do
            label 'Preview', class: 'label'
            span image_tag(f.object.logo_attachment)
            span link_to 'Remove', delete_logo_attachment_admin_setting_url(:id => f.object.id, :fallback_location => edit_admin_setting_path(f.object.id)),
                         method: :delete,
                         data: { confirm: 'Are you sure?' }
          end
        ) if f.object.logo_attachment.attached? )
      f.input :background_attachment, label: "Header Background", as: :file, :hint => (
        (
          li class: 'file input optional' do
            label 'Preview', class: 'label'
            span image_tag(f.object.background_attachment)
            span link_to 'Remove', delete_background_attachment_admin_setting_url(:id => f.object.id, :fallback_location => edit_admin_setting_path(f.object.id)),
                         method: :delete,
                         data: { confirm: 'Are you sure?' }
          end
        ) if f.object.background_attachment.attached? )
      f.input :title
      f.input :short_title
      f.input :description, as: :html_editor
      f.input :menu_1
      f.input :menu_2
      f.input :menu_3
      f.input :menu_5
      f.input :menu_6
      f.input :menu_7
      f.input :menu_8
      f.input :address
      f.input :email_address, label: "Email Address"
      f.input :contact_number_1, label: "Phone #"
      f.input :contact_number_2, label: "Landline #"
    end
    f.actions
  end

end
