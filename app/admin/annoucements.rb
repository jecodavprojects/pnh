ActiveAdmin.register Post, as: 'Announcements' do
  menu priority: 5, label: proc{ Setting.get_settings_value('menu_3') }

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  permit_params :title, :description, :user_id, :is_pinned, image_attachment: []
  #
  # or
  #
  # permit_params do
  #   permitted = [:title, :description, :user_id]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  controller do
    def scoped_collection
      end_of_association_chain.where(:section_id => 2).order('is_pinned ASC, created_at DESC')
    end
  end

  member_action :pin_post, method: :get do
    post = Post.find(params[:id])
    post.update_attributes(is_pinned: true)
    flash[:notice] = 'Successfully pinned a post'
    redirect_to admin_announcements_path
  end

  member_action :remove_pin_post, method: :get do
    post = Post.find(params[:id])
    post.update_attributes(is_pinned: false)
    flash[:notice] = 'Successfully removed pin on post'
    redirect_to admin_announcements_path
  end

  member_action :delete_image_attachment, method: :delete do
    ActiveStorage::Attachment.find(params[:id]).purge

    flash[:notice] = 'Successfully removed image attachment'
    redirect_back(fallback_location: params[:fallback_location])
  end

  index :title => proc{ Setting.get_settings_value('menu_3') } do
    column "Pin" do |p|
      if p.is_pinned
        span "Yes"
      end
    end
    column :title
    column "Description" do |p|
      truncate_html_length_to_sentence(raw(p.description), 500)
    end
    column "Created By" do |p|
      usr = AdminUser.find(p.user_id)
      link_to usr.email, admin_admin_user_path(p.user_id)
    end
    column :created_at
    actions defaults: true do |post|
      if post.is_pinned
        link_to "Remove Pin", remove_pin_post_admin_announcement_path(post), class: "member_link",
                method: :get,
                data: { confirm: 'Are you sure?' }
      else
        if Post.pinned_announcements.size < 3
          link_to "Pin Post", pin_post_admin_announcement_path(post), class: "member_link",
                  method: :get,
                  data: { confirm: 'Are you sure?' }
        end
      end
    end
  end

  show  do |post|
    attributes_table do
      row :title
      row "Created By" do |p|
        usr = AdminUser.find(p.user_id)
        link_to usr.email, admin_admin_user_path(p.user_id)
      end
      row :created_at
      row :updated_at
      row "Description" do |p|
        raw(p.description)
      end
    end

    panel "Image Attachment" do
      table_for post.image_attachment do
        column "Actions" do |p|
          span link_to 'Remove', delete_image_attachment_admin_announcement_url(:id => p.id, :fallback_location => admin_announcement_path(post.id)),
                       method: :delete,
                       data: { confirm: 'Are you sure?' }
        end
        column "Preview" do |p|
          span image_tag p
        end
        column :filename
        column :created_at
      end
    end
  end

  filter :title
  filter :description
  filter :user_id, :as => :select, :collection => AdminUser.all.map{|u| [u.email, u.id]}
  filter :created_at

  form :title => proc{ "Edit #{Setting.get_settings_value('menu_3')}" } do |f|
    f.inputs do
      f.input :image_attachment, label: "Attachment", as: :file, input_html: { multiple: true }
      f.input :title
      f.input :description, as: :html_editor
      f.input :section_id, :input_html => { :value => 2 }, as: :hidden
      f.input :user_id, :input_html => { :value => current_admin_user.id }, as: :hidden
      # f.input :is_pinned, label: "Pin this Post?"
    end
    f.actions

    panel "Image Attachment" do
      table_for f.object.image_attachment do
        column "Actions" do |p|
          span link_to 'Remove', delete_image_attachment_admin_announcement_url(:id => p.id, :fallback_location => admin_announcement_path(f.object.id)),
                       method: :delete,
                       data: { confirm: 'Are you sure?' }
        end
        column "Preview" do |p|
          span image_tag p
        end
        column :filename
        column :created_at
      end
    end
  end

end
