class UserMailer < ApplicationMailer
  default from: 'nsoepassamemail@gmail.com'

  def send_contact
    @name = params[:name]
    @phone = params[:phone]
    @email = params[:email]
    @message = params[:message]
    attachments.inline['logo.png'] = File.read("#{Rails.root}/app/assets/images/pnh.png")
    mail(
        :from => "nsoepassamemail@gmail.com",
        :to => Setting.get_settings_value('email_address'),
        :subject => "FAQ for User: #{@name} - #{@email}"
    )
  end

end