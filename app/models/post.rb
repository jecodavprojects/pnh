class Post < ApplicationRecord
  # 1 = Admission
  # 2 = Announcements
  # 3 = Facilities
  # 4 = Photo Gallery
  # 5 = About Us
  # 6 = Mission Vision
  has_many_attached :image_attachment

  def self.get_choices
   setting = Setting.first
   return [[1, setting.menu_2], [2, setting.menu_3], [3, setting.menu_4], [4, setting.menu_5], [5, setting.menu_7], [6, setting.menu_8]]
  end

  def self.admissions
    Post.where(section_id: 1).where('is_pinned = ? OR is_pinned IS NULL', false).order("created_at DESC")
  end

  def self.pinned_admissions
    Post.where(section_id: 1, is_pinned: true).order("created_at DESC")
  end

  def self.get_display_admissions
    pinned_size = pinned_admissions.size
    show_limit = 5 - pinned_size

    admissions.limit(show_limit)
  end

  def self.announcements
    Post.where(section_id: 2).where('is_pinned = ? OR is_pinned IS NULL', false).order("created_at DESC")
  end

  def self.pinned_announcements
    Post.where(section_id: 2, is_pinned: true).order("created_at DESC")
  end

  def self.get_display_announcements
    pinned_size = pinned_announcements.size
    show_limit = 5 - pinned_size

    announcements.limit(show_limit)
  end

  def self.facilities
    Post.where(section_id: 3).order("created_at DESC")
  end

  def self.photo_galleries
    Post.where(section_id: 4).order("created_at DESC")
  end

  def self.about_us
    Post.where(section_id: 5).last
  end

  def self.mission_vision
    Post.where(section_id: 6).last
  end
end
