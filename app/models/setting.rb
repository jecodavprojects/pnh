class Setting < ApplicationRecord
  has_one_attached :logo_attachment
  has_one_attached :background_attachment
  has_one_attached :image_attachment
  DEFAULTS = {
    "title" => "Passam National School of Excellence",
    "short_title" => "Panaths",
    "description" => "Educating Better Citizens For Tomorrow",
    "menu_1" => "Home",
    "menu_2" => "Admission",
    "menu_3" => "Announcements",
    "menu_4" => "Facilities",
    "menu_5" => "Photo Gallery",
    "menu_6" => "Contact Us",
    "menu_7" => "About Us",
    "menu_8" => "Mission & Vision",
    "address" => "P.O. Box 624, Wewak ESP Papua New Guinea",
    "email_address" => "passamnhs1978@gmail.com",
    "contact_number_1" => "(675) 71778773",
    "contact_number_2" => "(675) 78321611"
  }


  def self.get_settings_value(kval)
    # puts "working...."
    default_settings = Setting.first
    if default_settings
      if default_settings[kval]
          return default_settings[kval]
      end
    end

    return Setting::DEFAULTS[kval]
  end

end
