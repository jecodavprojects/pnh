module SettingsHelper
  def get_title
    get_settings_value('title')
  end
  def get_short_title
    get_settings_value('short_title')
  end

  def get_description
    get_settings_value('description')
  end

  def get_menu_1
    get_settings_value('menu_1')
  end

  def get_menu_2
    get_settings_value('menu_2')
  end

  def get_menu_3
    get_settings_value('menu_3')
  end

  def get_menu_4
    get_settings_value('menu_4')
  end

  def get_menu_5
    get_settings_value('menu_5')
  end

  def get_menu_6
    get_settings_value('menu_6')
  end

  def get_menu_7
    get_settings_value('menu_7')
  end

  def get_menu_8
    get_settings_value('menu_8')
  end

  def get_advisor_message
    get_settings_value('advisor_message')
  end

  def get_section_1
    get_settings_value('section_1')
  end

  def get_section_1_description
    get_settings_value('section_1_description')
  end

  def get_section_2
    get_settings_value('section_2')
  end

  def get_section_2_description
    get_settings_value('section_2_description')
  end

  def get_address
    get_settings_value('address')
  end

  def get_email_address
    get_settings_value('email_address')
  end

  def get_contact_number_1
    get_settings_value('contact_number_1')
  end

  def get_contact_number_2
    get_settings_value('contact_number_2')
  end

  def get_settings_value(kval)
    if default_settings
      if default_settings[kval]
          return default_settings[kval]
      end
    end

    return Setting::DEFAULTS[kval]
  end

  def default_settings
    Setting.first
  end
end
